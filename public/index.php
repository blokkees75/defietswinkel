<html lang="nl">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>De fietswinkel</title>
        <?php $page = "index"?>
        <link rel="stylesheet" href="styles/main.css" type="text/css">
        <link rel="stylesheet" href="styles/header.css" type="text/css">
    </head>
    <body>
        <?php include "./header.php" ?>
        <div class="center-container center-container-animated">
            <h1 class="center-container-h1-animated">De fiets&#8203winkel<h1>
        </div>
        <div class="custom-padding">
            <h2>Welkom. Wij zijn Kees, Niek en Floris. </h2>
            <p>Dit is de website die wij hebben gemaakt voor Informatica om dit onze kennis van php en erd’s toe te passen. Met deze website kan je records toevoegen en inzien in een perfecte, met CSS opgemaakte, website. </p>
            <br>
            <br>
            <h2>Klik hier of rechtsboven op <a class="blue" href="../customer">Klant</a> of <a class="blue" href="../supplier">Leverancier</a> om uw relatie met ons te kiezen.</h2>
        </div>
    </body>
</html>
