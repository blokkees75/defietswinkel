<html lang="nl">
    <head>
        <title>Klant</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php
            $page = "customer";
            $subpage = "rent";
        ?>
        <link rel="stylesheet" href="./styles/main.css" type="text/css">
        <link rel="stylesheet" href="./styles/header.css" type="text/css">
        <link rel="stylesheet" href="./styles/sidenav.css" type="text/css">
        <link rel="stylesheet" href="./styles/form.css" type="text/css">
    </head>
    <body>
        <?php include "./header.php" ?>
        <?php include "./sidenav.php" ?>

        <?php
            include '../src/database/database.php';
            include '../src/database/add.php';
            include '../src/database/get.php';

            $description = $bike = $handInTime = "";
            $descriptionErr = $bikeErr = $customerErr = $handInTimeErr = "";

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if (isset($_POST["Bevestigen"])) {
                    if (empty($_POST["bike"])) {
                        $bikeErr = "Geen fiets geselecteerd";
                    } else {
                        $bike = clean_data($_POST["bike"]);
                    }

                    if (empty($_POST["handInTime"])) {
                        $handInTimeErr = "Inleverdatum is verplicht";
                    } else {
                        $handInTime = clean_data($_POST["handInTime"]);
                    }

                    if (!empty($_POST["description"])) {
                        $description = clean_data($_POST["description"]);
                        // check description
                        if (strlen($description) >= 255) {
                            $descriptionErr = "Opmerking is te lang (max 255 karakters)";
                        }
                    }

                    if (!isset($_COOKIE["CustomerId"])) {
                        $customerErr = "U bent niet ingelogd<br><br>";
                    }

                    if ($bikeErr == "" && $handInTimeErr == "" && $descriptionErr == "" && $customerErr == "") {
                        // No errors, so submit the form
                        $db = db_connect();
                        $price = getBikeByFrameNumber($db, $bike)[0]["Uurprijs"];
                        addRent($db, $bike, $_COOKIE["CustomerId"], date('Y-m-d H:i:s'), $handInTime, $price, $description);
                        $db = null;
                    }
                }
            }

            function clean_data($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
        ?>

        <div class="content">
            <div class=" custom-padding">
                <h1>Fiets huren</h1>
                <p class="red">* verplicht</p>
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                    <label for="bike">Fiets <span class="red">*</span></label>
                    <select name="bike" id="bike">
                        <?php
                            $db = db_connect();
                            $bikes = getBikesNotRented($db);
                            $db = null;

                            foreach ($bikes as $bike) {
                                echo "<option value='".$bike["Framenummer"]."'>";
                                echo $bike["Merk"]." ".$bike["Type_fiets"]." ".$bike["Bouwjaar"]." maat ".$bike["Framemaat"]." ".$bike["Staat"]." ".$bike["Uurprijs"];
                                echo "</option>";
                            }
                        ?>
                    </select>
                    <span class="error"><?php echo $bikeErr;?></span>
                    <br>

                    <label for="handInTime">Inleverdatum <span class="red">*</span></label>
                    <input type="datetime-local" name="handInTime" value="<?php echo $handInTime?>">
                    <span class="error"><?php echo $handInTimeErr;?></span>
                    <br>
                    <br>

                    <label for="description">Opmerking</label>
                    <textarea name="description" value="<?php echo $description?>"></textarea>
                    <span class="error"><?php echo $descriptionErr;?></span>
                    <br>
                    <span class="error"><?php echo $customerErr;?></span>

                    <input type="reset" name="reset" onclick="window.location.reload()">

                    <input type="submit" name="Bevestigen" value="Bevestigen">
                </form>
            </div>
        </div>
    </body>
</html>