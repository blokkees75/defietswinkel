<html lang="nl">
    <head>
        <title>Verhuur</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php
            $page = "admin";
            $subpage = "rents";
        ?>
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/table.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
    </head>
    <body>
        <?php include "./header.php" ?>
        <?php include "./sidenav.php" ?>

        <div class="content">
            <div class="custom-padding">
            <h1>Verhuur</h1>

            <div class="table-parent">
                <table>
                    <tr>
                        <th>Verhuurnummer</th>
                        <th>Framenummer</th>
                        <th>Klantnummer</th>
                        <th>Uitleen tijdstip</th>
                        <th>tijdstip binnenkomst</th>
                        <th>Prijs</th>
                        <th>Opmerkingen</th>
                        <th>Betaaltijdstip</th>
                    </tr>
                    <?php
                        include '../../src/database/database.php';
                        include '../../src/database/get.php';
                    
                        $db = db_connect();
                        $rents = getRents($db);
                        $db = null;
                        
                        foreach ($rents as $rents) {
                            echo "<tr>";
                            foreach ($rents as $key => $value) {
                                    echo "<td>".$value."</td>";
                            }
                            echo "</tr>";
                        }
                    ?>
                </table>
            </div>
        </div>
    </div>
    </body>
</html>