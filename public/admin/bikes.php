<html lang="nl">
    <head>
        <title>Fietsen</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php
            $page = "admin";
            $subpage = "bikes";
        ?>
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/table.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
    </head>
    <body>
        <?php include "./header.php" ?>
        <?php include "./sidenav.php" ?>

        <div class="content">
            <div class="custom-padding">
                <h1>Fietsen</h1>

                <div class="table-parent">
                    <table>
                        <tr>
                            <th>Framenummer</th>
                            <th>Leveranciernummer</th>
                            <th>Merk</th>
                            <th>Type fiets</th>
                            <th>Bouwjaar</th>
                            <th>Framemaat</th>
                            <th>Omschrijving</th>
                            <th>Staat</th>
                            <th>Inkoopprijs</th>
                            <th>Uurprijs</th>   
                            <th>Verkoopprijs</th> 

                        </tr>
                        <?php
                            include '../../src/database/database.php';
                            include '../../src/database/get.php';
                        
                            $db = db_connect();
                            $bikes = getBikes($db);
                            $db = null;
                            
                            foreach ($bikes as $bike) {
                                echo "<tr>";
                                foreach ($bike as $key => $value) {
                                        echo "<td>".$value."</td>";
                                }
                                echo "</tr>";
                            }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>