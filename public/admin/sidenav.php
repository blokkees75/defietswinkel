<div class="sidebar">
    <a class="<?php echo $subpage == 'customers' ? 'active' : ''?>" href="../admin/customers.php">Klanten</a>
    <a class="<?php echo $subpage == 'suppliers' ? 'active' : ''?>" href="../admin/suppliers.php">Leveranciers</a>
    <a class="<?php echo $subpage == 'bikes' ? 'active' : ''?>" href="../admin/bikes.php">Fietsen</a>
    <a class="<?php echo $subpage == 'sales' ? 'active' : ''?>" href="../admin/sales.php">Verkoop</a>
    <a class="<?php echo $subpage == 'rents' ? 'active' : ''?>" href="../admin/rents.php">Verhuur</a>
    <a class="user-side" href="../index.php">Gebruiker weergave</a>
</div>