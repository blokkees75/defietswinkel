<html lang="nl">
    <head>
        <title>Klant</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php
            $page = "admin";
            $subpage = "customers";
        ?>
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/table.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
    </head>
    <body>
        <?php include "./header.php" ?>
        <?php include "./sidenav.php" ?>

        <div class="content">
            <div class="custom-padding">
                <h1>Klanten</h1>

                <div class="table-parent">
                    <table>
                        <tr>
                            <th>Id</th>
                            <th>Voornaam</th>
                            <th>Achternaam</th>
                            <th>Adres</th>
                            <th>Postcode</th>
                            <th>Telefoonnummer</th>
                            <th>Tijdstip aanmelding</th>
                            <th>E-mailadres</th>
                        </tr>
                        <?php
                            include '../../src/database/database.php';
                            include '../../src/database/get.php';
                        
                            $db = db_connect();
                            $customers = getCustomers($db);
                            $db = null;
                            
                            foreach ($customers as $customer) {
                                echo "<tr>";
                                foreach ($customer as $key => $value) {
                                    // if ($key !== "Datum_aanmelding") { // Don't display "Datum_aanmelding"
                                        echo "<td>".$value."</td>";
                                    // }
                                }
                                echo "</tr>";
                            }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>