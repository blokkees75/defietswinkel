<html lang="nl">
    <head>
        <title>Verkoop</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php
            $page = "admin";
            $subpage = "sales";
        ?>
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/table.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
    </head>
    <body>
        <?php include "./header.php" ?>
        <?php include "./sidenav.php" ?>

        <div class="content">
            <div class="custom-padding">
            <h1>Verkoop</h1>

            <div class="table-parent">
                <table>
                    <tr>
                        <th>Id</th>
                        <th>Framenummer</th>
                        <th>Klantnummer</th>
                        <th>Tijdstip</th>
                        <th>Prijs</th>
                        <th>Opmerkingen</th>
                        <th>Betaaltijdstip</th>
                    </tr>
                    <?php
                        include '../../src/database/database.php';
                        include '../../src/database/get.php';
                    
                        $db = db_connect();
                        $sales = getSales($db);
                        $db = null;
                        
                        foreach ($sales as $sales) {
                            echo "<tr>";
                            foreach ($sales as $key => $value) {
                                    echo "<td>".$value."</td>";
                            }
                            echo "</tr>";
                        }
                    ?>
                </table>
            </div>
        </div>
    </div>
    </body>
</html>