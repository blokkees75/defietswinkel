<html lang="nl">
    <head>
        <title>Leverancier</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php
            $page = "admin";
            $subpage = "suppliers";
        ?>
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/table.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
    </head>
    <body>
        <?php include "./header.php" ?>
        <?php include "./sidenav.php" ?>

        <div class="content">
            <div class="custom-padding">
            <h1>Leveranciers</h1>

            <div class="table-parent">
                <table>
                    <tr>
                        <th>Id</th>
                        <th>Naam</th>
                        <th>Adres</th>
                        <th>Email</th>
                        <th>Bankrekeningnummer</th>
                        <th>Postcode</th>
                    </tr>
                    <?php
                        include '../../src/database/database.php';
                        include '../../src/database/get.php';
                    
                        $db = db_connect();
                        $suppliers = getSuppliers($db);
                        $db = null;
                        
                        foreach ($suppliers as $supplier) {
                            echo "<tr>";
                            foreach ($supplier as $key => $value) {
                                    echo "<td>".$value."</td>";
                            }
                            echo "</tr>";
                        }
                    ?>
                </table>
            </div>
        </div>
    </div>
    </body>
</html>