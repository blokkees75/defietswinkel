<html lang="nl">
    <head>
        <title>Gekocht</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php 
        $page = "customer";
        $subpage = "sold";
        ?>
        <link rel="stylesheet" href="../../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../../styles/table.css" type="text/css">
        <link rel="stylesheet" href="../../styles/sidenav.css" type="text/css">
    </head>
    <body>
        <?php
            include ".././header.php";
            include ".././sidenav.php";

            if (isset($_COOKIE["CustomerId"])) {
                $customerNumber = $_COOKIE["CustomerId"];
            } else {                
                ob_start();
                header('Location: ../customer');
                ob_end_flush();
                die();
            }
        ?>

        <div class="content">
            <div class="custom-padding">
                <h1>Gekochte fietsen</h1>

                <div class="table-parent">
                    <table>
                        <tr>
                            <th>Framenummer</th>
                            <th>Datum</th>
                            <th>Prijs</th>
                            <th>Opmerkingen</th>
                            <th>Betaaltijdstip</th>
                        </tr>
                        <?php
                            include '../../src/database/database.php';
                            include '../../src/database/get.php';
                        
                            $db = db_connect();
                            $sales = getSaleByCustomernumber($db, $customerNumber);
                            $db = null;
                            
                            foreach ($sales as $sale) {
                                echo "<tr>";
                                foreach ($sale as $key => $value) {
                                    if ($key !== "Klantnummer" && $key !== "Verkoopnummer") { // Don't display "Klantnummer" and "Verkoopnummer"
                                        echo "<td>".$value."</td>";
                                    }
                                }
                                echo "</tr>";
                            }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>