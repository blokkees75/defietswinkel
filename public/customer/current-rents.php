<html lang="nl">
    <head>
        <title>Klant</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/form.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
        <link rel="stylesheet" href="../styles/table.css" type="text/css">
        <?php
            $page = "customer";
            $subpage = "current-rents";
        ?>
    </head>
    <body>

        <?php
            include '../../src/database/database.php';
            include '../../src/database/add.php';
            include '../../src/database/get.php';
            include '../../src/database/alter.php';
            include '../../src/database/delete.php';
            include "../header.php";
            include "../sidenav.php";
            // Variables for create account
            $frameNumber = $timeOut = $price = $handInTime = $payTime = $comments = "";
            $handInTimeErr = $commentsErr = "";

            // Extra variables
            $rentNumber = "";
            $rentNumberErr = "";

            if (!isset($_COOKIE["CustomerId"])) {                
                ob_start();
                header('Location: ../customer');
                ob_end_flush();
                die();
            }

            if (isset($_COOKIE["RentId"])) {
                $rentNumber = $_COOKIE["RentId"];
                $db = db_connect();
                $rent = getRent($db, $rentNumber)[0];
                $timeOut = $rent["Uitleen_tijdstip"];
                $handInTime = $rent["Inlever_tijdstip"];
                $handInTimeHTML = date('Y-m-d\TH:i', strtotime($handInTime));
                $price = $rent["Prijs"];
                $frameNumber = $rent["Framenummer"];
                $payTime = $rent["Betaaltijdstip"];
                $comments = $rent["Opmerkingen"];
            } else {
                $rentNumberErr = "Er is geen huur geselecteerd";
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if (isset($_POST["CreateRent"])) {
                    if (!empty($_POST["handInTimeHTML"])) {
                        $handInTimeHTML = clean_data($_POST["handInTimeHTML"]);
                    } else {
                        $handInTimeErr = "Inleverdatum is verplicht";
                    }

                    if (!empty($_POST["comments"])) {
                        $comments = clean_data($_POST["comments"]);
                        // check if comments only contains letters, whitespaces and numbers
                        if (!preg_match("/^[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.':-]+$/u",$comments) && $comments !== "") {
                            $commentsErr = "Ongeldige opmerking";
                        }
                    }

                    if ($handInTimeErr == "" && $commentsErr == "" && $rentNumberErr == "") {
                        // No errors, so submit the form
                        $db = db_connect();
                        alterRent($db, $rentNumber, $handInTime, $comments);
                    }
                }
                
                if(isset($_POST["returnBike"])){
                    $db = db_connect();
                    alterRentTimeBack($db, $rentNumber, date("Y:m:d H:i:s"));
                    alterRentPayTime($db, $rentNumber, date("Y:m:d H:i:s"));
                }
            }


            function clean_data($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
        ?>

        <div class="content">
            <div class="custom-padding">
                <div class="flex-container">
                    <div class= "flex-item">
                        <h1>Huidige huurgegevens</h1>
                        <p class="customer-tag">Huurnummer</p>
                            <?php echo ($rentNumberErr == "" ? "<p class='customer-data'>".$rentNumber."</p>" : "Er is geen huur geselecteerd")?> 
                            <br>
                            <br>
                        <p class="customer-tag">Framenummer</p>
                            <p class='customer-data'><?php echo $frameNumber ?></p> 
                            <br>
                            <br>
                        <p class="customer-tag">Uitleen tijdstip</p>
                            <p class='customer-data'><?php echo $timeOut ?></p> 
                            <br>
                            <br>
                        <p class="customer-tag">Inlever tijdstip</p>
                            <?php echo ($handInTimeErr == "" ? "<p class='customer-data'>".$handInTime."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="customer-tag">Uurprijs</p>
                            <p class='customer-data'><?php echo $price ?></p> 
                            <br>
                            <br>
                        <p class="customer-tag">Opmerkingen</p>
                            <?php echo ($commentsErr == "" ? "<p class='customer-data'>".$comments."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="customer-tag">Betaaltijdstip</p>
                            <p class='customer-data'><?php echo $payTime ?></p> 
                    </div>
                    <div class="flex-item">
                        <h1>Gegevens aanpassen</h1>
                        <p class="red">* verplicht</p>
                        <form id="createRentForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

                            <label for="handInTimeHTML">Inlevertijdstip<span class="red">*</span></label>
                            <input type="datetime-local" name="handInTimeHTML" value="<?php echo $handInTimeHTML?>">
                            <span class="red"><?php echo $handInTimeErr;?></span>
                            <br>
                            <br>

                            <label for="comments">Opmerkingen</label>
                            <input type="text" name="comments" value="<?php echo $comments?>">
                            <span class="red"><?php echo $commentsErr;?></span>
                            <br>
                            <br>   
                            <input type="reset" name="reset" onclick="window.location.reload()">

                            <input type="submit" name="CreateRent" value="Gegevens aanpassen">
                        </form>
                        <form id="createRentform" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                            <input type="submit" class="deleteAccount" name="returnBike" value="Fiets terugbrengen">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>