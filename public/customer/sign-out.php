<html lang="nl">
    <head>
        <title>Klant</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <?php $page = "customer"?>
    </head>
    <body>
        <?php include "../header.php" ?>

        <?php
            setcookie("CustomerId", "", time() - 3600, "/"); // set time to one hour ago (delete cookie)

            ob_start();
            header('Location: ../customer');
            ob_end_flush();
            die();
        ?>
    </body>
</html>