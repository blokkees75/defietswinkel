<html lang="nl">
    <head>
        <title>Gehuurd</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/table.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
        <link rel="stylesheet" href="../styles/form.css" type="text/css">
        <?php 
            $page = "customer";
            $subpage = "rented";
        ?>
    </head>
    <body>
        <?php 
            include "../header.php";
            include "../sidenav.php";
            include '../../src/database/database.php';
            include "../../src/database/get.php";

            $rentNumber = $customerNr = $customerNumber = "";
            $rentNumberErr = $customerErr = "";

            if (isset($_COOKIE["CustomerId"])) {
                $customerNumber = $_COOKIE["CustomerId"];
            } else {                
                ob_start();
                header('Location: ../customer');
                ob_end_flush();
                die();
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if (isset($_POST["SearchRent"])){
                    if (empty($_POST["rentNumber"])) {
                        $rentNumberErr = "Verhuurnummer is verplicht";
                    } else {
                        $rentNumber = clean_data($_POST["rentNumber"]);
                        // check for correct rent number
                        if (!preg_match("/^[0-9]+$/",$rentNumber)) { // 4294967295 is max value of int in mysql
                            $rentNumberErr = "Ongeldig verhuurnummer";
                        } else {
                            $db = db_connect();
                            $rent = getRent($db, $rentNumber)[0];
                            $customerNr = $rent["Klantnummer"];

                            if($customerNr !== "" && $customerNr == $customerNumber){
                                setcookie("RentId", strval($rentNumber), time() + (15 * 60), "/"); // 15 * 60 seconds = 15 minutes
                                ob_start();
                                header('Location: ../customer/current-rents.php');
                                ob_end_flush();
                                die();
                            } else if ($customerNr !== ""){
                                $rentNumberErr = "Dit verhuurnummer hoort bij een andere klant";
                            }
                        }
                    }
                }
            }

            function clean_data($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
        ?>


        <div class="content">
            <div class ="custom-padding">
                <h1>Gehuurde fietsen</h1>
                <div class="table-parent">
                    <table>
                        <tr>
                            <th>Verhuurnummer</th>
                            <th>Framenummer</th>
                            <th>Uitleendatum</th>
                            <th>Inleverdatum</th>
                            <th>Uurprijs</th>
                            <th>Opmerkingen</th>
                            <th>Betaaltijdstip</th>
                        </tr>
                        <?php                        
                            $db = db_connect();
                            $rents = getRentByCustomernumber($db, $customerNumber);
                            $db = null;
                            
                            foreach ($rents as $rent) {
                                echo "<tr>";
                                foreach ($rent as $key => $value) {
                                    if ($key !== "Klantnummer") { // Don't display "Klantnummer"
                                        echo "<td>".$value."</td>";
                                    }
                                }
                                echo "</tr>";
                            }
                        ?>
                    </table>
                </div>
                <br>
                <br>

                <h3>Voer een verhuurnummer in om een specifiek huurcontract te zien</h3>
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                    <label>Verhuurnummer</label>
                    <input type="number" name="rentNumber" value="<?php echo $rentNumber?>">
                    <span class="red"><?php echo $rentNumberErr;?></span>
                    <br>
                    <br>
                    <input type="submit" name="SearchRent" value="Zoeken">
                </form>
            </div>
        </div>
    </body>
</html>