<html lang="nl">
    <head>
        <title>Klant</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/form.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
        <link rel="stylesheet" href="../styles/table.css" type="text/css">
        <?php
            $page = "customer";
            $subpage = "account";
        ?>
    </head>
    <body>
        <?php 
        include "../header.php";
        include "../sidenav.php";
        ?>

        <?php
            include '../../src/database/database.php';
            include '../../src/database/add.php';
            include '../../src/database/get.php';
            include '../../src/database/alter.php';
            include '../../src/database/delete.php';

            // Variables for create account
            $fname = $lname = $address = $zip = $phone = $email = "";
            $fnameErr = $lnameErr = $addressErr = $zipErr = $phoneErr = $emailErr = "";

            // Extra variables
            $customerNumber = $signupDate = "";
            $customerNumberErr = "";

            if (isset($_COOKIE["CustomerId"])) {
                $customerNumber = $_COOKIE["CustomerId"];

                $db = db_connect();
                $customer = getCustomer($db, $customerNumber)[0];
                $signupDate = $customer["Datum_aanmelding"];
                $fname = $customer["Naam"];
                $lname = $customer["Achternaam"];
                $address = $customer["Adres"];
                $zip = $customer["Postcode"];
                $phone = $customer["Telefoonnummer"];
                $email = $customer["Emailadres"];
            } else {
                $customerNumberErr = "U bent niet ingelogd";
            }


            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if (!empty($_POST["CreateAccount"])) {

                    $fname = clean_data($_POST["fname"]);
                    // check if fname only contains letters and whitespace
                    if (!preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$fname)) {
                        $fnameErr = "Ongeldige voornaam";
                    }

                    $lname = clean_data($_POST["lname"]);
                    // check if lname only contains letters and whitespace
                    if (!preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$lname)) {
                        $lnameErr = "Ongeldige achternaam";
                    }

                    $address = clean_data($_POST["address"]);
                    // check if address only contains letters, whitespaces and numbers
                    if (!preg_match("/^[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$address)) {
                        $addressErr = "Ongeldig adres";
                    }

                    $zip = clean_data($_POST["zip"]);
                    // Set all characters to uppercase
                    $zip = strtoupper($zip);
                    // check if zip only contains letters, whitespaces and numbers
                    if (!preg_match("/^(?:NL-)?(\d{4})\s*([A-Z]{2})$/i",$zip)) {
                        $zipErr = "Ongeldige (Nederlandse) postcode";
                    }

                    $phone = clean_data($_POST["phone"]);
                    // check phonenumber
                    if (!preg_match("/^[+]?(?:\(\d+(?:\.\d+)?\)|\d+(?:\.\d+)?)(?:[ -]?(?:\(\d+(?:\.\d+)?\)|\d+(?:\.\d+)?))*(?:[ ]?(?:x|ext)\.?[ ]?\d{1,5})?$/",$phone)) {
                        $phoneErr = "Ongeldig telefoonnummer";
                    }

                    $email = clean_data($_POST["email"]);
                    // check email
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $emailErr = "Ongeldige E-mail";
                    }

                    if ($fnameErr == "" && $lnameErr == "" && $addressErr == "" && $zipErr == "" && $phoneErr == "" && $emailErr == "") {
                        // No errors, so submit the form
                        $db = db_connect();
                        AlterCustomer($db, $_COOKIE["CustomerId"], $fname, $lname, $address, $zip, $phone, $email);
                    }
                }
                
                if(!empty($_POST["deleteAccount"])){
                    $db = db_connect();
                    $succes = deleteCustomer($db, $_COOKIE["CustomerId"]);
                    
                    if ($succes) {
                        setcookie("CustomerId", "", time() - 3600, "/"); // set time to one hour ago (delete cookie)

                        ob_start();
                        header('Location: ../customer');
                        ob_end_flush();
                        die();
                    }
                }
            }

            function clean_data($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
        ?>

        <div class="content">
            <div class="custom-padding">
                <div class="flex-container">
                    <div class= "flex-item">
                        <h1>Uw huidige gegevens</h1>
                        <?php echo ($customerNumberErr !== "" ? "<p class='customer-data red'>".($customerNumberErr)."</p><br><br>" : "")?>
                        <p class="customer-tag">Klantnummer</p>
                            <?php echo ($customerNumberErr == "" ? "<p class='customer-data'>".$customerNumber."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="customer-tag">Voornaam</p>
                            <?php echo ($fnameErr == "" ? "<p class='customer-data'>".$fname."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="customer-tag">Achternaam</p>
                            <?php echo ($lnameErr == "" ? "<p class='customer-data'>".$lname."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="customer-tag">Adres</p>
                            <?php echo ($addressErr == "" ? "<p class='customer-data'>".$address."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="customer-tag">Postcode</p>
                            <?php echo ($zipErr == "" ? "<p class='customer-data'>".$zip."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="customer-tag">Telefoonnummer</p>
                            <?php echo ($phoneErr == "" ? "<p class='customer-data'>".$phone."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="customer-tag">E-mail</p>
                            <?php echo ($emailErr == "" ? "<p class='customer-data'>".$email."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="customer-tag">Klant sinds</p>
                            <p class="customer-data"><?php echo $signupDate?></p>
                    </div>
                    <div class="flex-item">
                        <h1>Gegevens aanpassen</h1>
                        <p class="red">* verplicht</p>
                        <form id="createCustomerForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                            <label for="fname">Voornaam <span class="red">*</span></label>
                            <input type="text" name="fname" value="<?php echo $fname?>">
                            <span class="red"><?php echo $fnameErr;?></span>
                            <br>
                            <br>

                            <label for="lname">Achternaam <span class="red">*</span></label>
                            <input type="text" name="lname" value="<?php echo $lname?>">
                            <span class="red"><?php echo $lnameErr;?></span>
                            <br>
                            <br>

                            <label for="address">Adres <span class="red">*</span></label>
                            <input type="text" name="address" value="<?php echo $address?>">
                            <span class="red"><?php echo $addressErr;?></span>
                            <br>
                            <br>

                            <label for="zip">Postcode <span class="red">*</span></label>
                            <input type="text" name="zip" value="<?php echo $zip?>">
                            <span class="red"><?php echo $zipErr;?></span>
                            <br>
                            <br>

                            <label for="phone">Telefoonnummer <span class="red">*</span></label>
                            <input type="tel" name="phone" value="<?php echo $phone?>">
                            <span class="red"><?php echo $phoneErr;?></span>
                            <br>
                            <br>

                            <label for="email">E-mail <span class="red">*</span></label>
                            <input type="email" name="email" value="<?php echo $email?>">
                            <span class="red"><?php echo $emailErr;?></span>
                            <br>
                            <br>
                            
                            
                            <input type="reset" name="reset" onclick="window.location.reload()">

                            <input type="submit" name="CreateAccount" value="Gegevens aanpassen">
                        </form>
                        <form id="createCustomerForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                            <input type="submit" class="deleteAccount" name="deleteAccount" value="Account verwijderen">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>