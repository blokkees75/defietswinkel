<html lang="nl">
    <head>
        <title>Klant</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/form.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
        <?php
            $page = "customer";
            $subpage = "sign-in-up-customer";
        ?>
    </head>
    <body>
        <?php 
        include "../header.php";
        include "../sidenav.php";
        ?>

        <?php
            include '../../src/database/database.php';
            include '../../src/database/add.php';
            include '../../src/database/get.php';

            // Variables for sign in
            $signInEmail = $signInEmailErr = "";

            // Variables for create account
            $fname = $lname = $address = $zip = $phone = $email = "";
            $fnameErr = $lnameErr = $addressErr = $zipErr = $phoneErr = $emailErr = "";

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                // Sign in
                if (!empty($_POST["SignIn"])) {
                    if (empty($_POST["email"])) {
                        $signInEmailErr = "E-mail is verplicht";
                    } else {
                        $signInEmail = clean_data($_POST["email"]);
                        // check email
                        if (!filter_var($signInEmail, FILTER_VALIDATE_EMAIL)) {
                            $signInEmailErr = "Ongeldige E-mail";
                        }
                    }

                    if ($signInEmailErr == "") {
                        signIn($signInEmail);
                    }
                }

                // Create account
                if (!empty($_POST["CreateAccount"])) {
                    if (empty($_POST["fname"])) {
                        $fnameErr = "Voornaam is verplicht";
                    } else {
                        $fname = clean_data($_POST["fname"]);
                        // check if fname only contains letters and whitespace
                        if (!preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$fname)) {
                            $fnameErr = "Ongeldige voornaam";
                        }
                    }

                    if (empty($_POST["lname"])) {
                        $lnameErr = "Achternaam is verplicht";
                    } else {
                        $lname = clean_data($_POST["lname"]);
                        // check if lname only contains letters and whitespace
                        if (!preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$lname)) {
                            $lnameErr = "Ongeldige achternaam";
                        }
                    }

                    if (empty($_POST["address"])) {
                        $addressErr = "Adres is verplicht";
                    } else {
                        $address = clean_data($_POST["address"]);
                        // check if address only contains letters, whitespaces and numbers
                        if (!preg_match("/^[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$address)) {
                            $addressErr = "Ongeldig adres";
                        }
                    }

                    if (empty($_POST["zip"])) {
                        $zipErr = "Postcode is verplicht";
                    } else {
                        $zip = clean_data($_POST["zip"]);
                        // Set all characters to uppercase
                        $zip = strtoupper($zip);
                        // check if zip only contains letters, whitespaces and numbers
                        if (!preg_match("/^(?:NL-)?(\d{4})\s*([A-Z]{2})$/i",$zip)) {
                            $zipErr = "Ongeldige (Nederlandse) postcode";
                        }
                    }

                    if (empty($_POST["phone"])) {
                        $phoneErr = "Telefoonnummer is verplicht";
                    } else {
                        $phone = clean_data($_POST["phone"]);
                        // check phonenumber
                        if (!preg_match("/^[+]?(?:\(\d+(?:\.\d+)?\)|\d+(?:\.\d+)?)(?:[ -]?(?:\(\d+(?:\.\d+)?\)|\d+(?:\.\d+)?))*(?:[ ]?(?:x|ext)\.?[ ]?\d{1,5})?$/",$phone)) {
                            $phoneErr = "Ongeldig telefoonnummer";
                        }
                    }

                    if (empty($_POST["email"])) {
                        $emailErr = "E-mail is verplicht";
                    } else {
                        $email = clean_data($_POST["email"]);
                        // check email
                        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $emailErr = "Ongeldige E-mail";
                        }
                    }

                    if ($fnameErr == "" && $lnameErr == "" && $addressErr == "" && $zipErr == "" && $phoneErr == "" & $emailErr == "") {
                        // No errors, so submit the form
                        $db = db_connect();
                        $succes = addCustomer($db, $fname, $lname, $address, $zip, $phone, $email);
                        $db = null;

                        if ($succes) {
                            signIn($email);
                        }
                    }
                }
            }

            function clean_data($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }

            function signIn($email) {
                $db = db_connect();
                $customerIds = getCustomerByEmail($db, $email);
                $db = null;

                if (count($customerIds) == 0) {
                    echo "E-mailadres is niet bekend";
                } else {
                    $customerId = reset(array_values($customerIds)[0]);
                    setcookie("CustomerId", strval($customerId), time() + (15 * 60), "/"); // 15 * 60 seconds = 15 minutes

                    ob_start();
                    header('Location: ../customer');
                    ob_end_flush();
                    die();
                }
            }
        ?>
        <div class="content">
            <div class="custom-padding">
                <div class="flex-container">
                    <div class="flex-item">
                        <h1>Inloggen</h1>
                        <p class="red">* verplicht</p>
                        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                            <label for="signInEmail">E-mail <span class="red">*</span></label>
                            <input type="email" name="email" value="<?php echo $signInEmail?>">
                            <span class="error"><?php echo $signInEmailErr;?></span>
                            <br>
                            <br>

                            <input type="reset" name="reset" onclick="window.location.reload()">

                            <input type="submit" name="SignIn" value="Log in">
                        </form>
                    </div>

                    <div class="flex-item">
                        <h1>Klantenaccount aanmaken</h1>
                        <p class="red">* verplicht</p>
                        <form id="createCustomerForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                            <label for="fname">Voornaam <span class="red">*</span></label>
                            <input type="text" name="fname" value="<?php echo $fname?>">
                            <span class="error"><?php echo $fnameErr;?></span>
                            <br>
                            <br>

                            <label for="lname">Achternaam <span class="red">*</span></label>
                            <input type="text" name="lname" value="<?php echo $lname?>">
                            <span class="error"><?php echo $lnameErr;?></span>
                            <br>
                            <br>

                            <label for="address">Adres <span class="red">*</span></label>
                            <input type="text" name="address" value="<?php echo $address?>">
                            <span class="error"><?php echo $addressErr;?></span>
                            <br>
                            <br>

                            <label for="zip">Postcode <span class="red">*</span></label>
                            <input type="text" name="zip" value="<?php echo $zip?>">
                            <span class="error"><?php echo $zipErr;?></span>
                            <br>
                            <br>

                            <label for="phone">Telefoonnummer <span class="red">*</span></label>
                            <input type="tel" name="phone" value="<?php echo $phone?>">
                            <span class="error"><?php echo $phoneErr;?></span>
                            <br>
                            <br>

                            <label for="email">E-mail <span class="red">*</span></label>
                            <input type="email" name="email" value="<?php echo $email?>">
                            <span class="error"><?php echo $emailErr;?></span>
                            <br>
                            <br>

                            
                            <input type="reset" name="reset" onclick="window.location.reload()">

                            <input type="submit" name="CreateAccount" value="Account aanmaken">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>