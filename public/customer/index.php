<html lang="nl">
    <head>
        <title>Klant</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php
            $page = "customer";
            $subpage = "";
        ?>
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
    </head>
    <body>
        <?php include "../header.php" ?>
        <?php include "../sidenav.php" ?>

        <?php
            // redirect if not signed in
            if(!isset($_COOKIE["CustomerId"])) {
                ob_start();
                header('Location: ../customer/sign-in-up.php');
                ob_end_flush();
                die();
            }
        ?>

        <div class="custom-padding">
        </div>
    </body>
</html>