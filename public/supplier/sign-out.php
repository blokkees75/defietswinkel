<html lang="nl">
    <head>
        <title>Klant</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php $page = "supplier"?>
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
    </head>
    <body>
        <?php include "../header.php" ?>

        <?php
            setcookie("SupplierId", "", time() - 3600, "/"); // set time to one hour ago (delete cookie)

            ob_start();
            header('Location: ../supplier');
            ob_end_flush();
            die();
        ?>
    </body>
</html>