<html lang="nl">
    <head>
        <title>Leverancier</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php
            $page = "supplier";
            $subpage = "new-bike";
        ?>
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/form.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
    </head>
    <body>
        <?php include "../header.php" ?>
        <?php include "../supplier/sidenav.php" ?>

        <?php
            include '../../src/database/database.php';
            include '../../src/database/add.php';
            include '../../src/database/get.php';

            $frameNumber = $bikeType = $brand = $buildYear = $frameSize = $description = $bikeCondition = $price = $retailPrice = $rentPrice = "";
            $frameNumberErr = $bikeTypeErr = $brandErr = $buildYearErr = $frameSizeErr = $descriptionErr = $bikeConditionErr = $priceErr = $retailPriceErr = $rentPriceErr = $supplierErr = "";
            
            // redirect if not signed in
            if(!isset($_COOKIE["SupplierId"])) {
                ob_start();
                header('Location: ../supplier/sign-in-up.php');
                ob_end_flush();
                die();
            }

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                
                if (isset($_POST["Bevestigen"])) {

                    if (empty($_POST["frameNumber"])) {
                        $frameNumberErr = "framenummer is verplicht";
                    } else {
                        $frameNumber = clean_data($_POST["frameNumber"]);
                        $frameNumber = strtoupper($frameNumber);
                        // check if input matches the required amount and order of letters and numbers. MOET NOG GEDAAN WORDEN
                        if (!preg_match("/^[A-Z]{2}[0-9]{7}$/", $frameNumber)) {
                            $frameNumberErr = "Ongeldig framenummer";
                        }
                    }

                    if (empty($_POST["brand"])) {
                        $brandErr = "Merk is verplicht";
                    } else {
                        $brand = clean_data($_POST["brand"]);
                        // check if input does not contain weird letters.
                        if (!preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$brand)) {
                            $brandErr = "Ongeldig merk";
                        }
                    }

                    if (empty($_POST["bikeType"])) {
                        $bikeType = "type fiets is verplicht";
                    } else {
                        $bikeType = clean_data($_POST["bikeType"]);
                    }

                    if (empty($_POST["buildYear"])) {
                        $buildYearErr = "Bouwjaar is verplicht";
                    } else {
                        $buildYear = clean_data($_POST["buildYear"]);
                        // check if input matches valid year
                        if (!preg_match("/^[0-9]{4}$/", $buildYear) || $buildYear < 2000 || $buildYear > 2100) {
                            $buildYearErr = "Ongeldig bouwjaar (2000 - 2100)";
                        }
                    }

                    if (empty($_POST["frameSize"])) {
                        $frameSizeErr = "framemaat is verplicht";
                    } else {
                        $frameSize = clean_data($_POST["frameSize"]);
                        // check if input matches realistic framesize
                        if (!preg_match("/^[0-9]{2}$/", $frameSize) || $frameSize < 43 || $frameSize > 65) {
                            $frameSizeErr = "Ongeldige framemaat (43 - 65)";
                        }
                    }

                    if (!empty($_POST["description"])) {
                         $description = clean_data($_POST["description"]);
                        // check if input does not contain weird letters
                        if (!preg_match("/^[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.':-]+$/u", $description)) {
                            $descriptionErr = "Ongeldige opmerking, speciale tekens zijn niet toegestaan.";
                        }
                    }

                    if (empty($_POST["bikeCondition"])) {
                        $bikeConditionErr = "Staat is verplicht";
                    } else {
                        $bikeCondition = clean_data($_POST["bikeCondition"]);
                    }

                    if (empty($_POST["price"])) {
                        $priceErr = "Inkoopprijs is verplicht";
                    } else {
                        $price = clean_data($_POST["price"]);
                    }

                    if (empty($_POST["retailPrice"])) {
                        $retailPrice = null;
                    } else {
                        $retailPrice = clean_data($_POST["retailPrice"]);
                    }

                    if (empty($_POST["rentPrice"])) {
                        $rentPrice = null;
                    } else {
                        $rentPrice = clean_data($_POST["rentPrice"]);
                    }

                    if (empty($_POST["retailPrice"]) && empty($_POST["rentPrice"]) || !empty($_POST["retailPrice"]) && !empty($_POST["rentPrice"])){
                        $rentPriceErr = "Er moet één veld ingevuld worden van advies- en huurprijs";
                    }

                    if (!isset($_COOKIE["SupplierId"])) {
                        $supplierErr = "U bent niet ingelogd<br><br>";
                    }

                    if ($frameNumberErr == "" && $bikeTypeErr == "" && $brandErr =="" && $buildYearErr == "" && $frameSizeErr == "" && $descriptionErr =="" && $bikeConditionErr =="" && $priceErr == "" && $retailPriceErr == "" && $rentPriceErr == "" && $supplierErr == "") {
                        // No errors, so submit the form
                        $db = db_connect();
                        addBike($db, $frameNumber, $_COOKIE["SupplierId"], $brand, $bikeType, $buildYear, $frameSize, $description, $bikeCondition, $price, $retailPrice, $rentPrice);
                        $db = null;
                    }
                }
            }
            
            function clean_data($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }

        ?>

        <div class="content">
            <div class="custom-padding">
                <h1>Nieuwe fiets aanleveren</h1>
                <p class="red">* verplicht</p>
                <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                    <label for="frameNumber">Framenummer <span class="red">*</span></label>
                    <input type="text" name="frameNumber" value="<?php echo $frameNumber?>">
                    <span class="red"><?php echo $frameNumberErr;?></span>
                    <br><br>

                    <label for="brand">Merk <span class="red">*</span></label>
                    <input type="text" name="brand" value="<?php echo $brand?>">
                    <span class="red"><?php echo $brandErr;?></span>
                    <br><br>

                    <label for="bikeType">Fietstype <span class="red">*</span></label>
                        <select name="bikeType" id="bikeType" class="enum">
                            <option value="stadsfiets">Stadsfiets</option>
                            <option value="elektrische fiets">Elektrische fiets</option>
                            <option value="mountainbike">Mountainbike</option>
                            <option value="tourfiets">Tourfiets</option>
                            <option value="wielrenfiets">Wielrenfiets</option>
                            <option value="hybride fiets">Hybride fiets</option>
                        </select>
                    <span class="red"><?php echo $bikeTypeErr;?></span>
                    <br><br>

                    <label for="buildYear">Bouwjaar <span class="red">*</span></label> 
                    <input type="number" min="2000" max="2100" name="buildYear" value="<?php echo $buildYear?>">
                    <span class="red"><?php echo $buildYearErr;?></span>
                    <br><br>

                    <label for="frameSize">Framemaat <span class="red">*</span></label> 
                    <input type="number" name="frameSize" min="43" max="65" value="<?php echo $frameSize?>">
                    <span class="red"><?php echo $frameSizeErr;?></span>
                    <br><br>

                    <label for="description">Omschrijving</label>
                    <textarea name="description" value="<?php echo $description?>"></textarea>
                    <span class="red"><?php echo $descriptionErr;?></span>
                    <br><br>

                    <label for="bikeCondition">Staat <span class="red">*</span> </label> 
                        <select name ="bikeCondition" id="bikeCondition" class="enum">
                            <option value="nieuw">Nieuw</option>
                            <option value="gebruikerssporen">Gebruikerssporen</option>
                            <option value="gebruikt">Gebruikt</option>
                            <option value="intensief gebruikt">Intensief gebruikt</option>
                        </select>
                    <span class="red"><?php echo $bikeConditionErr;?></span>
                    <br><br>

                    <label for="price">Inkoopprijs <span class="red">*</span></label>
                    <input type="number" min="0" step="0.01" name="price" value="<?php echo $price?>">
                    <span class="red"><?php echo $priceErr;?></span>
                    <br><br>

                    <label for="retailPrice">Adviesprijs</label>
                    <input type="number" min="0" step="0.01" name="retailPrice" value="<?php echo $retailPrice?>">
                    <span class="red"><?php echo $retailPriceErr;?></span>
                    <br><br>

                    <label for="rentPrice">Huurprijs </label>
                    <input type="number" min="0" step="0.01" name="rentPrice" value="<?php echo $rentPrice?>">
                    <span class="red"><?php echo $rentPriceErr;?></span>
                    <br><br>

                    <span class="red"><?php echo $supplierErr;?></span>
                    <input type="reset" name="reset" onclick="window.location.reload()">

                    <input type="submit" name="Bevestigen" value="Bevestigen">
                </form>
            </div>
        </div>
    </body>
</html>