<html lang="nl">
    <head>
        <title>Geleverde fietsen</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <?php 
        $page = "supplier";
        $subpage = "supplied-bikes";
        ?>
        <link rel="stylesheet" href="../../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../../styles/table.css" type="text/css">
        <link rel="stylesheet" href="../../styles/sidenav.css" type="text/css">
    </head>
    <body>
        <?php include ".././header.php" ?>
        <?php include "../supplier/sidenav.php"?>
        <div class="content">
            <div class="custom-padding">
                <h1>Geleverde fietsen</h1>

                <div class="table-parent">
                    <table>
                        <tr>
                            <th>Framenummer</th>
                            <th>Merk</th>
                            <th>Type fiets</th>
                            <th>Bouwjaar</th>
                            <th>Framemaat</th>
                            <th>Omschrijving</th>
                            <th>Staat</th>
                            <th>Inkooopprijs</th>
                            <th>Uurprijs</th>
                            <th>Adviesprijs</th>
                        </tr>
                        <?php
                            include '../../src/database/database.php';
                            include '../../src/database/get.php';
                        
                            $db = db_connect();
                            $supplies = getBikeBySuppliernumber($db, $_COOKIE["SupplierId"]);
                            $db = null;
                            
                            foreach ($supplies as $supply) {
                                echo "<tr>";
                                foreach ($supply as $key => $value) {
                                    if ($key !== "Leveranciernummer") { // Don't display "Leveranciernummer"
                                        echo "<td>".$value."</td>";
                                    }
                                }
                                echo "</tr>";
                            }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>