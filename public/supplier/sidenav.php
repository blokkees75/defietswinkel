<div class="sidebar">
    <?php
        if (isset($_COOKIE["SupplierId"])) {
            echo '<a class="signout" href="../supplier/sign-out.php">Uitloggen</a>';
            echo '<a class="'.($subpage == 'new-bike' ? 'active' : '').'" href="../supplier/index.php">Fiets aanleveren</a>';
            echo '<a class="'.($subpage == 'supplied-bikes' ? 'active' : '').'" href="../supplier/bikes.php">geleverde fietsen</a>';
            echo "<a class='account-nav ".($subpage == 'account' ? 'active' : '')."' href='../supplier/account.php'>Account</a>";
        } else {
            echo '<a class="signin '.($subpage == 'sign-in-up-supplier' ? 'active' : '').'" href="../supplier/sign-in-up.php">Inloggen</a>';
        }
    ?>
</div>