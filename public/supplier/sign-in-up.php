<html lang="nl">
    <head>
        <title>Leverancier</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  

        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/form.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">

        <?php
            $page = "supplier";
            $subpage = "sign-in-up-supplier";
        ?>
    </head>
    <body>
        <?php
            include "../header.php";
            include "../supplier/sidenav.php"
        ?>

        <?php
            include '../../src/database/database.php';
            include '../../src/database/add.php';
            include '../../src/database/get.php';

            // Variables for sign in
            $signInEmail = $signInEmailErr = "";

            $companyName = $address = $zip = $email = $bankAcc = "";
            $companyNameErr = $addressErr = $zipErr = $emailErr = $bankAccErr = "";

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                // Sign in
                if (!empty($_POST["SignIn"])) {
                    if (empty($_POST["email"])) {
                        $signInEmailErr = "E-mail is verplicht";
                    } else {
                        $signInEmail = clean_data($_POST["email"]);
                        // check email
                        if (!filter_var($signInEmail, FILTER_VALIDATE_EMAIL)) {
                            $signInEmailErr = "Ongeldige E-mail";
                        }
                    }

                    if ($signInEmailErr == "") {
                        signIn($signInEmail);
                    }
                }

                //create account
                if (!empty($_POST["CreateAccount"])) {
                    if (empty($_POST["companyName"])) {
                        $companyNameErr = "Bedrijfsnaam is verplicht";
                    } else {
                        $companyName = clean_data($_POST["companyName"]);
                        // check if companyName only contains letters and whitespace
                        if (!preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$companyName)) {
                            $companyNameErr = "Ongeldige bedrijfsnaam";
                        }
                    }

                    if (empty($_POST["address"])) {
                        $addressErr = "Adres is verplicht";
                    } else {
                        $address = clean_data($_POST["address"]);
                        // check if address only contains letters, whitespaces and numbers
                        if (!preg_match("/^[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$address)) {
                            $addressErr = "Ongeldig adres";
                        }
                    }

                    if (empty($_POST["zip"])) {
                        $zipErr = "Postcode is verplicht";
                    } else {
                        $zip = clean_data($_POST["zip"]);
                        // Set all characters to uppercase
                        $zip = strtoupper($zip);
                        // check if zip only contains letters, whitespaces and numbers
                        if (!preg_match("/^(?:NL-)?(\d{4})\s*([A-Z]{2})$/",$zip)) {
                            $zipErr = "Ongeldige (Nederlandse) postcode";
                        }
                    }

                    if (empty($_POST["bankAcc"])) {
                        $bankAccErr = "IBAN is verplicht";
                    } else {
                        $bankAcc = clean_data($_POST["bankAcc"]);
                        $bankAcc = strtoupper ($bankAcc);
                        $bankAcc = str_replace(' ','',$bankAcc);
                        // check bankAccnumber
                        if (!preg_match("/^[A-Z]{2}[0-9]{2}(?:[ ]?[A-Z]{4}){1}(?:[ ]?[0-9]{4}){2}(?:[ ]?[0-9]{1,2})?$/", $bankAcc)) {
                            $bankAccErr = "Ongeldige IBAN";
                        }
                    }

                    if (empty($_POST["email"])) {
                        $emailErr = "E-mail is verplicht";
                    } else {
                        $email = clean_data($_POST["email"]);
                        // check email
                        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $emailErr = "Ongeldige E-mail";
                        }
                    }

                    if ($companyNameErr == "" && $addressErr == "" && $zipErr == "" && $bankAccErr == "" & $emailErr == "") {
                        // No errors, so submit the form
                        $db = db_connect();
                        $succes = addSupplier($db, $companyName, $address, $zip, $email, $bankAcc);
                        $db = null;
                        
                        if($succes){
                            signIn($email);
                        }
                    }
                }
            }

            function clean_data($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }

            function signIn($email) {
                $db = db_connect();
                $supplierIds = getSupplierByEmail($db, $email);
                $db = null;

                if (count($supplierIds) == 0) {
                    echo "E-mailadres is niet bekend";
                } else {
                    $supplierId = reset(array_values($supplierIds)[0]);
                    setcookie("SupplierId", strval($supplierId), time() + (15 * 60), "/"); // 15 * 60 seconds = 15 minutes

                    ob_start();
                    header('Location: ../supplier');
                    ob_end_flush();
                    die();
                }
            }
        ?>

        <div class="content">
            <div class="custom-padding">
                <div class="flex-container">
                    <div class="flex-item">
                        <h1>Inloggen</h1>
                        <p class="red">* verplicht</p>
                        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                            <label for="signInEmail">E-mail <span class="red">*</span></label>
                            <input type="email" name="email" value="<?php echo $signInEmail?>">
                            <span class="error"><?php echo $signInEmailErr;?></span>
                            <br>
                            <br>

                            <input type="reset" name="reset" onclick="window.location.reload()">

                            <input type="submit" name="SignIn" value="Log in">
                        </form>
                    </div>

                    <div class="flex-item">
                        <h1>Leverancieraccount aanmaken</h1>
                        <p class="red">* verplicht</p>
                        <form id="createSupplierForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                            <label for="companyName">Bedrijfsnaam <span class="red">*</span></label>
                            <input type="text" name="companyName" value="<?php echo $companyName?>">
                            <span class="error"><?php echo $companyNameErr;?></span>
                            <br>
                            <br>

                            <label for="address">Adres <span class="red">*</span></label>
                            <input type="text" name="address" value="<?php echo $address?>">
                            <span class="error"><?php echo $addressErr;?></span>
                            <br>
                            <br>

                            <label for="zip">Postcode <span class="red">*</span></label>
                            <input type="text" name="zip" value="<?php echo $zip?>">
                            <span class="error"><?php echo $zipErr;?></span>
                            <br>
                            <br>

                            <label for="bankAcc">IBAN <span class="red">*</span></label>
                            <input type="text" name="bankAcc" value="<?php echo $bankAcc?>">
                            <span class="error"><?php echo $bankAccErr;?></span>
                            <br>
                            <br>

                            <label for="email">E-mail <span class="red">*</span></label>
                            <input type="email" name="email" value="<?php echo $email?>">
                            <span class="error"><?php echo $emailErr;?></span>
                            <br>
                            <br>

                            
                            <input type="reset" name="reset" onclick="window.location.reload()">

                            <input type="submit" name="CreateAccount" value="Account aanmaken">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>