<html lang="nl">
    <head>
        <title>Leverancier</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <link rel="stylesheet" href="../styles/main.css" type="text/css">
        <link rel="stylesheet" href="../styles/header.css" type="text/css">
        <link rel="stylesheet" href="../styles/form.css" type="text/css">
        <link rel="stylesheet" href="../styles/sidenav.css" type="text/css">
        <link rel="stylesheet" href="../styles/table.css" type="text/css">
        <?php
            $page = "supplier";
            $subpage = "account";
        ?>
    </head>
    <body>
        <?php 
        include "../header.php";
        include "../supplier/sidenav.php";
        ?>

        <?php
            include '../../src/database/database.php';
            include '../../src/database/add.php';
            include '../../src/database/get.php';
            include '../../src/database/alter.php';
            include '../../src/database/delete.php';

            // Variables for create account
            $name = $address = $email = $bankacc = $zip = "";
            $nameErr = $addressErr = $emailErr = $bankaccErr = $zipErr = "";

            // Extra variables
            $supplierNumber = "";
            $supplierNumberErr = "";

            if (isset($_COOKIE["SupplierId"])) {
                $supplierNumber = $_COOKIE["SupplierId"];

                $db = db_connect();
                $supplier = getSupplier($db, $supplierNumber)[0];
                $name = $supplier["Naam"];
                $address = $supplier["Adres"];
                $email = $supplier["Email"];
                $zip = $supplier["Postcode"];
                $bankacc = $supplier["Bankrekeningnummer"];
            } else {
                $supplierNumberErr = "U bent niet ingelogd";
            }


            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                if (!empty($_POST["CreateAccount"])) {

                    $name = clean_data($_POST["name"]);
                    // check if fname only contains letters and whitespace
                    if (!preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$name)) {
                        $nameErr = "Ongeldige voornaam";
                    }

                    $address = clean_data($_POST["address"]);
                    // check if address only contains letters, whitespaces and numbers
                    if (!preg_match("/^[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u",$address)) {
                        $addressErr = "Ongeldig adres";
                    }

                    $zip = clean_data($_POST["zip"]);
                    // Set all characters to uppercase
                    $zip = strtoupper($zip);
                    // check if zip only contains letters, whitespaces and numbers
                    if (!preg_match("/^(?:NL-)?(\d{4})\s*([A-Z]{2})$/i",$zip)) {
                        $zipErr = "Ongeldige (Nederlandse) postcode";
                    }

                    $bankacc = clean_data($_POST["bankacc"]);
                    // check phonenumber
                    if (!preg_match("/^[A-Z]{2}[0-9]{2}(?:[ ]?[A-Z]{4}){1}(?:[ ]?[0-9]{4}){2}(?:[ ]?[0-9]{1,2})?$/",$bankacc)) {
                        $bankaccErr = "Ongeldige IBAN";
                    }

                    $email = clean_data($_POST["email"]);
                    // check email
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $emailErr = "Ongeldige E-mail";
                    }

                    if ($nameErr == "" && $addressErr == "" && $zipErr == "" && $bankaccErr == ""  && $emailErr == "") {
                        // No errors, so submit the form
                        $db = db_connect();
                        AlterSupplier($db, $_COOKIE["SupplierId"], $name, $address, $email, $bankacc, $zip);
                    }
                }
                
                if(!empty($_POST["deleteAccount"])){
                    $db = db_connect();
                    $succes = deleteSupplier($db, $_COOKIE["SupplierId"]);
                    
                    if ($succes) {
                        setcookie("SupplierId", "", time() - 3600, "/"); // set time to one hour ago (delete cookie)

                        ob_start();
                        header('Location: ../supplier');
                        ob_end_flush();
                        die();
                    }
                }
            }

            function clean_data($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
        ?>

        <div class="content">
            <div class="custom-padding">
                <div class="flex-container">
                    <div class= "flex-item">
                        <h1>Uw huidige gegevens</h1>
                        <?php echo ($supplierNumberErr !== "" ? "<p class='supplier-data red'>".($supplierNumberErr)."</p><br><br>" : "")?>
                        <p class="supplier-tag">Leveranciernummer</p>
                            <p class="supplier-data"><?php echo $supplierNumber?></p>
                            <br>
                            <br>
                        <p class="supplier-tag">Naam</p>
                            <?php echo ($nameErr == "" ? "<p class='supplier-data'>".$name."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="supplier-tag">Adres</p>
                            <?php echo ($addressErr == "" ? "<p class='supplier-data'>".$address."</p>" : "")?> 
                            <br>
                            <br>
                        <p class="supplier-tag">Postcode</p>
                            <?php echo ($zipErr == "" ? "<p class='supplier-data'>".$zip."</p>" : "")?>
                            <br>
                            <br>
                        <p class="supplier-tag">IBAN</p>
                            <?php echo ($bankaccErr == "" ? "<p class='supplier-data'>".$bankacc."</p>" : "")?>
                            <br>
                            <br>
                        <p class="supplier-tag">E-mail</p>
                            <?php echo ($emailErr == "" ? "<p class='supplier-data'>".$email."</p>" : "")?>

                    </div>
                    <div class="flex-item">
                        <h1>Gegevens aanpassen</h1>
                        <p class="red">* verplicht</p>
                        <form id="createSupplierForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                            <label for="name">Naam <span class="red">*</span></label>
                            <input type="text" name="name" value="<?php echo $name?>">
                            <span class="red"><?php echo $nameErr;?></span>
                            <br>
                            <br>

                            <label for="address">Adres <span class="red">*</span></label>
                            <input type="text" name="address" value="<?php echo $address?>">
                            <span class="red"><?php echo $addressErr;?></span>
                            <br>
                            <br>

                            <label for="zip">Postcode <span class="red">*</span></label>
                            <input type="text" name="zip" value="<?php echo $zip?>">
                            <span class="red"><?php echo $zipErr;?></span>
                            <br>
                            <br>

                            <label for="bankacc">IBAN <span class="red">*</span></label>
                            <input type="text" name="bankacc" value="<?php echo $bankacc?>">
                            <span class="red"><?php echo $bankaccErr;?></span>
                            <br>
                            <br>

                            <label for="email">E-mail <span class="red">*</span></label>
                            <input type="email" name="email" value="<?php echo $email?>">
                            <span class="red"><?php echo $emailErr;?></span>
                            <br>
                            <br>
                            
                            
                            <input type="reset" name="reset" onclick="window.location.reload()">

                            <input type="submit" name="CreateAccount" value="Gegevens aanpassen">
                        </form>
                        <form id="createSupplierForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                            <input type="submit" class="deleteAccount" name="deleteAccount" value="Account verwijderen">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>