<div class="sidebar">
    <a class="<?php echo $subpage == 'rent' ? 'active' : ''?>" href="../rent.php">Huren</a>
    <a class="<?php echo $subpage == 'sale' ? 'active' : ''?>" href="../sale.php">Kopen</a>
    <?php
        if (isset($_COOKIE["CustomerId"])) {
            echo "<a class='".($subpage == 'sold' ? 'active' : '')."' href='../customer/sold.php'>Gekocht</a>";
            echo "<a class='".($subpage == 'rented' ? 'active' : '')."' href='../customer/rented.php'>Gehuurd</a>";
            echo "<a class='account-nav ".($subpage == 'account' ? 'active' : '')."' href='../customer/account.php'>Account</a>";
            echo '<a class="signout" href="../customer/sign-out.php">Uitloggen</a>';
        } else {
            echo '<a class="signin '.($subpage == 'sign-in-up-customer' ? 'active' : '').'" href="../customer/sign-in-up.php">Inloggen</a>';
        }
    ?>
</div>