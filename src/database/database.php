<?php

include 'keys/database_conninfo.inc.php';

function db_connect(): PDO
{
    global $db_host, $db_database, $db_username, $db_password;

    try {
        $conn = new PDO(
            "mysql:host=$db_host;dbname=$db_database",
            $db_username,
            $db_password,
            array(
                PDO::MYSQL_ATTR_SSL_KEY => './keys/client-key.pem',
                PDO::MYSQL_ATTR_SSL_CERT => './keys/client-cert.pem',
                PDO::MYSQL_ATTR_SSL_CA => './keys/ca-cert.pem',
                PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false
            )
        );
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        die("Could not connect to database: " . $e->getMessage());
    }

    return $conn;
}