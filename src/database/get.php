<?php

function getCustomers($db) {
    try {
        $query = $db->prepare("SELECT * FROM `Klant`");
        $query->execute();
        $customers = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get customers: " . $e->getMessage());
    }

    return $customers;
}

function getCustomer($db, $id) {
    try {
        $query = $db->prepare("SELECT * FROM `Klant` WHERE `Klantnummer`=:id");
        $query->bindParam("id", $id);
        $query->execute();
        $customer = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get customer: " . $e->getMessage());
    }

    return $customer;
}

function getCustomerByEmail($db, $email) {
    try {
        $query = $db->prepare("SELECT `Klantnummer` FROM `Klant` WHERE `Emailadres`=:email");
        $query->bindParam("email", $email);
        $query->execute();
        $customer = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get customer: " . $e->getMessage());
    }

    return $customer;
}

function getSuppliers($db) {
    try {
        $query = $db->prepare("SELECT * FROM `Leverancier`");
        $query->execute();
        $supplier = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get suppliers: " . $e->getMessage());
    }

    return $supplier;
}

function getSupplier($db, $id) {
    try {
        $query = $db->prepare("SELECT * FROM `Leverancier` WHERE `Leveranciernummer`=:id");
        $query->bindParam("id", $id);
        $query->execute();
        $supplier = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get supplier: " . $e->getMessage());
    }

    return $supplier;
}

function getSupplierByEmail($db, $email) {
    try {
        $query = $db->prepare("SELECT `Leveranciernummer` FROM `Leverancier` WHERE `Email`=:email");
        $query->bindParam("email", $email);
        $query->execute();
        $customer = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get Supplier: " . $e->getMessage());
    }

    return $customer;
}

function getBikes($db) {
    try {
        $query = $db->prepare("SELECT * FROM `Fiets`");
        $query->execute();
        $bikes = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get bikes: " . $e->getMessage());
    }

    return $bikes;
}

 function getBikeByFrameNumber($db, $framenummer) {
    try {
        $query = $db->prepare("SELECT * FROM `Fiets` WHERE `Framenummer`=:Framenummer");
        $query->bindParam("Framenummer", $framenummer);
        $query->execute();
        $bike = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get bike: " . $e->getMessage());
    }

    return $bike;
}

 function getBikeBySuppliernumber($db, $Leveranciernummer) {
    try {
        $query = $db->prepare("SELECT * FROM `Fiets` WHERE `Leveranciernummer`=:Leveranciernummer");
        $query->bindParam("Leveranciernummer", $Leveranciernummer);
        $query->execute();
        $bike = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get bike: " . $e->getMessage());
    }

    return $bike;
}


function getBikesNotSold($db) {
    try{
        $query = $db->prepare("SELECT * FROM Fiets WHERE `Adviesprijs` IS NOT NULL AND `Framenummer` NOT IN (SELECT `Framenummer` FROM `Verkoop`)");
        $query->execute();
        $bike = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get bikes: ". $e->getMessage());
    }

    return $bike;
}

function getBikesNotRented($db) {
    try{
        $query = $db->prepare('SELECT *
                                FROM Fiets
                                WHERE `Uurprijs` IS NOT NULL
                                AND `Framenummer` NOT IN (
                                    SELECT `Framenummer`
                                    FROM `Verhuur`
                                    WHERE `Inlever_tijdstip` > NOW()
                                    OR `Inlever_tijdstip` IS NULL
                                )');
        $query->execute();
        $bike = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get bikes: ". $e->getMessage());
    }

    return $bike;
}

function getSales($db) {
    try {
        $query = $db->prepare("SELECT * FROM `Verkoop`");
        $query->execute();
        $sales = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get sales: " . $e->getMessage());
    }

    return $sales;
}

 function getSaleByCustomernumber($db, $Klantnummer) {
    try {
        $query = $db->prepare("SELECT * FROM `Verkoop` WHERE `Klantnummer`=:Klantnummer");
        $query->bindParam("Klantnummer", $Klantnummer);
        $query->execute();
        $sale = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get sale: " . $e->getMessage());
    }

    return $sale;
}

function getSaleByFramenumber($db, $Framenummer) {
    try {
        $query = $db->prepare("SELECT * FROM `Verkoop` WHERE `Framenummer`=:Framenummer");
        $query->bindParam("Framenummer", $Framenummer);
        $query->execute();
        $sale = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get sale: " . $e->getMessage());
    }

    return $sale;
}

function getRents($db) {
    try {
        $query = $db->prepare("SELECT * FROM `Verhuur`");
        $query->execute();
        $rents = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get rents: " . $e->getMessage());
    }

    return $rents;
}

 function getRentByFramenumber($db, $Framenummer) {
    try {
        $query = $db->prepare("SELECT * FROM `Verhuur` WHERE `Framenummer`=:Framenummer");
        $query->bindParam("Framenummer", $Framenummer);
        $query->execute();
        $rent = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get rent: " . $e->getMessage());
    }

    return $rent;
}

function getRentByCustomernumber($db, $Klantnummer) {
    try {
        $query = $db->prepare("SELECT * FROM `Verhuur` WHERE `Klantnummer`=:Klantnummer");
        $query->bindParam("Klantnummer", $Klantnummer);
        $query->execute();
        $rent = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get rent: " . $e->getMessage());
    }

    return $rent;
}

function getRent($db, $rentNumber) {
    try {
        $query = $db->prepare("SELECT * FROM `Verhuur` WHERE `Verhuurnummer`=:rentNumber");
        $query->bindParam("rentNumber", $rentNumber);
        $query->execute();
        $rent = $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        die("Could not get rent: " . $e->getMessage());
    }

    return $rent;
}