CREATE TABLE Leverancier
(
    Leveranciernummer   INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Naam			    VARCHAR(255) 			    NOT NULL,
    Adres 				VARCHAR(255)                NOT NULL,
    Postcode            VARCHAR(255)                NOT NULL,
    Email  				VARCHAR(255)                NOT NULL,
    Bankrekeningnummer	VARCHAR(255)                NOT NULL
);

CREATE TABLE Fiets
(
    Framenummer         VARCHAR(255)        UNIQUE  PRIMARY KEY,
    Leveranciernummer   INT UNSIGNED,
    Merk 			    VARCHAR(255)                NOT NULL,
    Type_fiets  	    ENUM(
                            'stadsfiets',
                            'elektrische fiets',
                            'mountainbike',
                            'tourfiets',
                            'wielrenfiets',
                            'hybride fiets'
                        )                           NOT NULL,
    Bouwjaar		    YEAR                        NOT NULL,
    Framemaat		    TINYINT                     NOT NULL,
    Omschrijving	    VARCHAR(255),
    Staat			    ENUM(
                            'nieuw',
                            'gebruikerssporen',
                            'gebruikt',
                            'intensief gebruikt'
                        )                           NOT NULL,
    Inkoopprijs         DECIMAL(6,2)                NOT NULL,
    Adviesprijs        DECIMAL(6,2),
    Uurprijs            DECIMAL(6,2),
    
    FOREIGN KEY (Leveranciernummer) REFERENCES Leverancier (Leveranciernummer) ON DELETE SET NULL
);

CREATE TABLE Klant
(
    Klantnummer         INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    Naam                VARCHAR(255)                NOT NULL,
    Achternaam          VARCHAR(255)                NOT NULL,
    Adres               VARCHAR(255)                NOT NULL,
    Postcode            VARCHAR(255)                NOT NULL,
    Telefoonnummer      VARCHAR(255)                NOT NULL,
    Datum_aanmelding    TIMESTAMP   DEFAULT CURRENT_TIMESTAMP   NOT NULL,
    Emailadres          VARCHAR(255)                NOT NULL
);

CREATE TABLE Verkoop
(
    Verkoopnummer       INT UNSIGNED        AUTO_INCREMENT      PRIMARY KEY,
    Framenummer         VARCHAR(255),
    Klantnummer         INT UNSIGNED,
    Tijdstip            TIMESTAMP   DEFAULT CURRENT_TIMESTAMP   NOT NULL,
    Prijs               DECIMAL(6,2)                            NOT NULL,
    Opmerkingen         VARCHAR(255),
    Betaaltijdstip      TIMESTAMP,
    
    FOREIGN KEY (Framenummer) REFERENCES Fiets (Framenummer) ON DELETE SET NULL,
    FOREIGN KEY (Klantnummer) REFERENCES Klant (Klantnummer) ON DELETE SET NULL
);

CREATE TABLE Verhuur
(
    Verhuurnummer       INT UNSIGNED AUTO_INCREMENT             PRIMARY KEY,
    Framenummer         VARCHAR(255),
    Klantnummer         INT UNSIGNED,
    Uitleen_tijdstip    TIMESTAMP   DEFAULT CURRENT_TIMESTAMP   NOT NULL,
    Inlever_tijdstip    TIMESTAMP,
    Prijs               DECIMAL(6,2),
    Opmerkingen         VARCHAR(255),
    Betaaltijdstip      TIMESTAMP,
    
    FOREIGN KEY (Framenummer) REFERENCES Fiets (Framenummer) ON DELETE SET NULL,
    FOREIGN KEY (Klantnummer) REFERENCES Klant (Klantnummer) ON DELETE SET NULL
);