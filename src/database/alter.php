<?php

function alterSalePrice($db, $salesNumber, $price){
    try {
        $query = $db->prepare("UPDATE Verkoop SET Prijs = :price WHERE verkoopnummer = :salesNumber");
        $query->bindParam("price", $price);
        $query->bindParam("salesNumber", $salesNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            return false;
            echo "Er is een fout opgetreden!";
        }
    } catch (PDOException $e) {
        die("Could not Alter sale: " . $e->getMessage());
    }
    return true;
}

function alterSaleComment($db, $salesNumber, $comments){
    try {
        $query = $db->prepare("UPDATE Verkoop SET Opmerkingen = :comments WHERE verkoopnummer = :salesNumber");
        $query->bindParam("comments", $comments);
        $query->bindParam("salesNumber", $salesNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter sale: " . $e->getMessage());
    }
    return true;
}

function alterSaleTime($db, $salesNumber, $time){
    try {
        $query = $db->prepare("UPDATE Verkoop SET Betaaltijdstip = :saleTime WHERE verkoopnummer = :salesNumber");
        $query->bindParam("saleTime", $time);
        $query->bindParam("salesNumber", $salesNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter sale: " . $e->getMessage());
    }
    return true;
}

function alterBikeDescription($db, $frameNumber, $description){
    try {
        $query = $db->prepare("UPDATE Fiets SET Omschrijving = :bikeDescription WHERE framenummer = :frameNumber");
        $query->bindParam("bikeDescription", $description);
        $query->bindParam("frameNumber", $frameNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter bike: " . $e->getMessage());
    }
    return true;
}

function alterBikeCondition($db, $frameNumber, $condition){
    try {
        $query = $db->prepare("UPDATE Fiets SET Staat = :condition WHERE framenummer = :frameNumber");
        $query->bindParam("condition", $condition);
        $query->bindParam("frameNumber", $frameNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter bike: " . $e->getMessage());
    }
    return true;
}

function alterBikeSalePrice($db, $frameNumber, $price){
    try {
        $query = $db->prepare("UPDATE Fiets SET Verkoopprijs = :price WHERE framenummer = :frameNumber");
        $query->bindParam("price", $price);
        $query->bindParam("frameNumber", $frameNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter bike: " . $e->getMessage());
    }
    return true;
}

function alterBikeRentPrice($db, $frameNumber, $rentPrice){
    try {
        $query = $db->prepare("UPDATE Fiets SET Uurprijs = :rentPrice WHERE framenummer = :frameNumber");
        $query->bindParam("rentPrice", $rentPrice);
        $query->bindParam("frameNumber", $frameNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter bike: " . $e->getMessage());
    }
    return true;
}

function alterRentTimeBack($db, $rentNumber, $timeBack){
    try {
        $query = $db->prepare("UPDATE Verhuur SET Inlever_tijdstip = :timeBack WHERE verhuurnummer = :rentNumber");
        $query->bindParam("timeBack", $timeBack);
        $query->bindParam("rentNumber", $rentNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter rent: " . $e->getMessage());
    }
    return true;
}

function alterRentPrice($db, $rentNumber, $price){
    try {
        $query = $db->prepare("UPDATE Verhuur SET Prijs = :price WHERE verhuurnummer = :rentNumber");
        $query->bindParam("price", $price);
        $query->bindParam("rentNumber", $rentNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter rent: " . $e->getMessage());
    }
    return true;
}

function alterRentComments($db, $rentNumber, $comments){
    try {
        $query = $db->prepare("UPDATE Verhuur SET Opmerkingen = :comments WHERE verhuurnummer = :rentNumber");
        $query->bindParam("comments", $comments);
        $query->bindParam("rentNumber", $rentNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter rent: " . $e->getMessage());
    }
    return true;
}

function alterRentPayTime($db, $rentNumber, $payTime){
    try {
        $query = $db->prepare("UPDATE Verhuur SET Betaaltijdstip = :payTime WHERE verhuurnummer = :rentNumber");
        $query->bindParam("payTime", $payTime);
        $query->bindParam("rentNumber", $rentNumber);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter rent: " . $e->getMessage());
    }
    return true;
}


function alterCustomer($db, $customerId, $firstname = null, $surname  = null, $adress  = null, $postalCode  = null, $phoneNumber  = null, $emailAdress  = null){
    try {
        $query = $db->prepare("UPDATE Klant SET Naam=:firstname, Achternaam=:surname, Adres=:adress, Postcode=:postalCode, Telefoonnummer=:phoneNumber, Emailadres=:emailAdress WHERE Klantnummer = :customerId");
        $query->bindParam("firstname", $firstname);
        $query->bindParam("customerId", $customerId);
        $query->bindParam("surname", $surname);
        $query->bindParam("adress", $adress);
        $query->bindParam("postalCode", $postalCode);
        $query->bindParam("phoneNumber", $phoneNumber);
        $query->bindParam("emailAdress", $emailAdress);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter customer: " . $e->getMessage());
    }
    return true;
}

function alterSupplier($db, $supplierId, $name = null, $adress  = null, $emailAdress  = null, $bankacc = null, $zip  = null){
    try {
        $query = $db->prepare("UPDATE Leverancier SET Naam=:name, Adres=:adress, Email=:emailAdress, Bankrekeningnummer=:bankacc, Postcode=:zip  WHERE Leveranciernummer = :supplierId");
        $query->bindParam("name", $name);
        $query->bindParam("supplierId", $supplierId);
        $query->bindParam("adress", $adress);
        $query->bindParam("zip", $zip);
        $query->bindParam("emailAdress", $emailAdress);
        $query->bindParam("bankacc", $bankacc);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter supplier: " . $e->getMessage());
    }
    return true;
}

function alterRent($db, $rentId, $timeBack, $comments = NULL){
    try {
        $query = $db->prepare("UPDATE Verhuur SET Inlever_tijdstip=:timeBack, Opmerkingen=:comments WHERE Verhuurnummer = :rentId");
        $query->bindParam("rentId", $rentId);
        $query->bindParam("timeBack", $timeBack);
        $query->bindParam("comments", $comments);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not alter rent: " . $e->getMessage());
    }
    return true;
}