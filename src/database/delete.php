<?php

function deleteCustomer($db, $customerNumber) {
    try {
        $query = $db->prepare("DELETE FROM Klant WHERE Klantnummer = :customerNumber");
        $query->bindParam("customerNumber", $customerNumber);
        if($query->execute()) {
            echo "De gegevens zijn verwijderd.";
        } else {
            echo "Er is een fout opgetreden!"; 
            return false;
        }
    } catch (PDOException $e) {
        die("Could not delete customer: " . $e->getMessage());
    }
    return true;
}

function deleteBike($db, $frameNumber) {
    try {
        $query = $db->prepare("DELETE FROM Fiets WHERE Framenummer = :frameNumber");
        $query->bindParam("frameNumber", $frameNumber);
        if($query->execute()) {
            echo "De gegevens zijn verwijderd.";
        } else {
            echo "Er is een fout opgetreden!"; 
            return false;
        }
    } catch (PDOException $e) {
        die("Could not delete bike: " . $e->getMessage());
    }
    return true;
}

function deleteRent($db, $rentNumber) {
    try {
        $query = $db->prepare("DELETE FROM Verhuur WHERE Verhuurnummer = :rentNumber");
        $query->bindParam("rentNumber", $rentNumber);
        if($query->execute()) {
            echo "De gegevens zijn verwijdert.";
        } else {
            echo "Er is een fout opgetreden!"; 
            return false;
        }
    } catch (PDOException $e) {
        die("Could not delete renting: " . $e->getMessage());
    }
    return true;
}

function deleteSale($db, $saleNumber) {
    try {
        $query = $db->prepare("DELETE FROM Verkoop WHERE Verkoopnummer = :saleNumber");
        $query->bindParam("saleNumber", $saleNumber);
        if($query->execute()) {
            echo "De gegevens zijn verwijdert.";
        } else {
            echo "Er is een fout opgetreden!"; 
            return false;
        }
    } catch (PDOException $e) {
        die("Could not delete sale: " . $e->getMessage());
    }
    return true;
}

function deleteSupplier($db, $supplierNumber) {
    try {
        $query = $db->prepare("DELETE FROM Leverancier WHERE Leveranciernummer = :supplierNumber");
        $query->bindParam("supplierNumber", $supplierNumber);
        if($query->execute()) {
            echo "De gegevens zijn verwijdert.";
        } else {
            echo "Er is een fout opgetreden!"; 
            return false;
        }
    } catch (PDOException $e) {
        die("Could not delete supplier: " . $e->getMessage());
    }
    return true;
}

