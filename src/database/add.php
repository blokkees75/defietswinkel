<?php

function addCustomer($db, $firstname, $surname, $adress, $postalCode, $phoneNumber, $emailAdress) {
    try {
        $query = $db->prepare("INSERT INTO Klant (Naam, Achternaam, Adres, Postcode, Telefoonnummer, Emailadres) VALUES (:firstname, :surname, :adress, :postalCode, :phoneNumber, :emailAdress)");
        $query->bindParam("firstname", $firstname);
        $query->bindParam("surname", $surname);
        $query->bindParam("adress", $adress);
        $query->bindParam("postalCode", $postalCode);
        $query->bindParam("phoneNumber", $phoneNumber);
        $query->bindParam("emailAdress", $emailAdress);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!"; 
            return false;
        }
    } catch (PDOException $e) {
        die("Could not add customer: " . $e->getMessage());
    }
    return true;
}

function addSale($db, $frameNumber, $customerNumber, $price, $comments = null, $payTime = null) {
    try {
        $query = $db->prepare("INSERT INTO Verkoop (Framenummer, Klantnummer, Prijs, Opmerkingen, Betaaltijdstip) VALUES (:frameNumber, :customerNumber, :price, :comments, :payTime)"); 
        $query->bindParam("frameNumber", $frameNumber);
        $query->bindParam("customerNumber", $customerNumber);
        $query->bindParam("price", $price);
        $query->bindParam("comments", $comments);
        $query->bindParam("payTime", $payTime);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!"; 
            return false;
        }
    } catch (PDOException $e) {
        die("Could not add sale: " . $e->getMessage());
    }
    return true;
}

function addRent($db, $frameNumber, $customerNumber, $rentOutTime, $rentInTime, $price, $comments = null, $payTime = null) {
    try {
        $query = $db->prepare("INSERT INTO Verhuur (Framenummer, Klantnummer, Uitleen_tijdstip, Inlever_tijdstip, Prijs, Opmerkingen, Betaaltijdstip) VALUES (:frameNumber, :customerNumber, :rentOutTime, :rentInTime, :price, :comments, :payTime)");
        $query->bindParam("frameNumber", $frameNumber);
        $query->bindParam("customerNumber", $customerNumber);
        $query->bindParam("rentOutTime", $rentOutTime);
        $query->bindParam("rentInTime", $rentInTime);
        $query->bindParam("price", $price);
        $query->bindParam("comments", $comments);
        $query->bindParam("payTime", $payTime);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not add rent: " . $e->getMessage());
    }
    return true;
}

function addBike($db, $frameNumber, $supplierNumber, $brand, $bikeType, $buildYear, $frameSize, $descriptions = null, $condition, $supplierPrice, $salePrice = null, $hourPrice = null) {
    try {
        $query = $db->prepare("INSERT INTO Fiets (Framenummer, Leveranciernummer, Merk, Type_fiets, Bouwjaar, Framemaat, Omschrijving, Staat, Inkoopprijs, Adviesprijs, Uurprijs) VALUES (:frameNumber, :supplierNumber, :brand, :bikeType, :buildYear, :frameSize, :descriptions, :condition, :supplierPrice, :salePrice, :hourPrice)");
        $query->bindParam("frameNumber", $frameNumber);
        $query->bindParam("supplierNumber", $supplierNumber);
        $query->bindParam("brand", $brand);
        $query->bindParam("bikeType", $bikeType);
        $query->bindParam("buildYear", $buildYear);
        $query->bindParam("frameSize", $frameSize);
        $query->bindParam("descriptions", $descriptions);
        $query->bindParam("condition", $condition);
        $query->bindParam("supplierPrice", $supplierPrice);
        $query->bindParam("salePrice", $salePrice);
        $query->bindParam("hourPrice", $hourPrice);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not add bike: " . $e->getMessage());
    }
    return true;
}

function addSupplier($db, $companyName, $adress, $postalCode, $emailAdress, $bankAccount) {
    try {
        $query = $db->prepare("INSERT INTO Leverancier (Naam, Adres, Postcode, Email, Bankrekeningnummer) VALUES (:companyName, :adress, :postalCode, :emailAdress, :bankAccount)");
        $query->bindParam("companyName", $companyName);
        $query->bindParam("adress", $adress);
        $query->bindParam("postalCode", $postalCode);
        $query->bindParam("emailAdress", $emailAdress);
        $query->bindParam("bankAccount", $bankAccount);
        if($query->execute()) {
            echo "De nieuwe gegevens zijn toegevoegd.";
        } else {
            echo "Er is een fout opgetreden!";
            return false;
        }
    } catch (PDOException $e) {
        die("Could not add supplier: " . $e->getMessage());
    }
    return true;
}