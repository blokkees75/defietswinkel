# De fietswinkel

## Een informaticaproject voor school

## Wat te doen om een connectie met de database te kunnen maken
- Zet in de map [src/database/keys](src/database/keys) de drie .pem bestanden
    - ca-cert.pem
    - client-cert.pem
    - client-key.pem
- Zet in deze map ook het bestand database_conninfo.inc.php

## Wat te doen om de website te openen
Voer het volgende uit in de terminal:
```shell
cd public/
php -S localhost:8000 router.php
```